extends KinematicBody2D

export (int) var speed= 200
const GRABITATEA = 10

var norantza = 1
var velocity = Vector2()

func hil():
    queue_free()
    
func _physics_process(delta):
    velocity.y += GRABITATEA
    velocity.x = speed * norantza
    $AnimatedSprite.play("ibili")
    move_and_slide(velocity,Vector2(0, -1))
    
    if is_on_wall():
        norantza = norantza * -1
        
    if norantza == -1:
        $AnimatedSprite.flip_h = false
    else:
        $AnimatedSprite.flip_h = true
func _on_Timer_timeout():
    $CollisionShape2D.disabled = true
    queue_free()
    
func _on_Area2D_body_entered(body):
    if "Jokalaria" in body.name:
        body.mina_jaso()