extends KinematicBody2D
export (int) var speed = 200
const GRABITATEA = 10
var norantza = 1
var energia = 3
export (int) var salto = 470

const BALA = preload("res://bala.tscn")

var velocity = Vector2()

func get_input():
    velocity.x = 0
    if Input.is_action_pressed('Right'):
        velocity.x = speed
        $AnimatedSprite.flip_h = false
        norantza = 1
    if Input.is_action_pressed('Left'):
        velocity.x = -speed
        $AnimatedSprite.flip_h = true
        norantza = -1
    if Input.is_action_pressed('up'):
        if is_on_floor():
            velocity.y = -salto
    if Input.is_action_just_pressed("fire"):
        var bala = BALA.instance()
        get_parent().add_child(bala)
        bala.set_norantza(norantza)
        bala.position = $Position2D.global_position

func _physics_process(delta):
    get_input()
    velocity.y += GRABITATEA
    if velocity.x !=0:
        $AnimatedSprite.play("ibili")
    else:
        $AnimatedSprite.play("geldi")
    move_and_slide(velocity,Vector2(0,-1))
func eguneratu_energia():
    get_tree().get_root().get_node("Nagusia").find_node("Energia").set_text("Energia" + str(energia))
    
func mina_jaso(mina=1):
    energia -= mina
    if energia < 1:
        queue_free()
        get_tree().quit()
