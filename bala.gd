extends Area2D

const SPEED = 150
var velocity = Vector2()
var norantza = 1

func set_norantza(nor):
    norantza = nor
    if norantza == 1:
        $AnimatedSprite.flip_h = false
    else:
        $AnimatedSprite.flip_h = true

func _physics_process(delta):
    velocity.x = SPEED * delta * norantza
    translate(velocity)
    $AnimatedSprite.play("default")
    
func _on_bala_body_entered(body):
    if "malo" in body.name:
        body.hil()
        queue_free()